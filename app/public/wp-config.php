<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '32vIBCi36Tyud8554QV/1QI47V0kyboaDOhUzSLkuyDAQsJ1vDfLVGtfFimzEYkOEj1SbQguJHjpXoQmnqSYFA==');
define('SECURE_AUTH_KEY',  '1SnLeevRR/Kik1sb2DFTLCOuPDfIUXuoiYdqYPWkzhTL+lJaVdYyJYwSh08HBdN3urL813Xl6eQ7UfxPkZMZOQ==');
define('LOGGED_IN_KEY',    'sxy2l22pT6gMjBt/bFgHA3TvSKlaVBOazzJsqTxxMUc7aq5M5pTtTBYtPP6j3NJEKo9dyVrNnCGuf4pJ2uk2vA==');
define('NONCE_KEY',        'aG0Akdg+P9TIHqooeHcKEvGEmwRcetGfoLT4rpkVE/kAu7ZuHR9Hfg/C0HU3lhVeyRMr1uFo2r79s7D3I46DSQ==');
define('AUTH_SALT',        'bzUHGDK8Q/eO+/IOWQzMKBHDO+VvNuT0dELxN6VwB4HWkgBpuBe/lukZ5mmzgwEW2At+edJJBXYb20ur0GRrrA==');
define('SECURE_AUTH_SALT', 'd4dU04KcMdmPvxRTb92b9ggwK/DvPqWGTZOVk4PT2NeefHRgr5usfMpSqCMINBjpn4kIhzd3QKErCLF9HOPong==');
define('LOGGED_IN_SALT',   'VYTzai1Qv7BNFDdc58pzPeJ1pLgJXumw5Uz2yaAqL3KJFbzh1UdNw3WnmK3qhhC6eS/4JLyrR3VHsW3K2HgAYQ==');
define('NONCE_SALT',       'yJii10USWDalhBvoEQ/VCROoi3mo1lUj3zFtPp7TsX75nvvyjlP1XU/dfdlSQ1WIon8VlQG3fw1PMFYL6jh0Mw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
